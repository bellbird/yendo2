import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import {Bienvenida} from '../pages/bienvenida/bienvenida';

import {Principal} from '../pages/principal/principal';
import {Login} from '../pages/login/login';
import { Password } from '../pages/password/password';
import { Registrarse } from '../pages/registrarse/registrarse';
import { Busqueda} from '../pages/busqueda/busqueda';
import { DetalleLocal } from  '../pages/detalle-local/detalle-local';

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    Bienvenida,
    Login,
    Principal,
    Password,
    Registrarse,
    Busqueda,
    DetalleLocal,


  ],
  imports: [
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    Bienvenida,
    Login,
    Principal,
    Password,
    Registrarse,
    Busqueda,
    DetalleLocal,
  ],
  providers: []
})
export class AppModule {}
