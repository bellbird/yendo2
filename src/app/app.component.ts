import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import {Bienvenida} from '../pages/bienvenida/bienvenida'
import {Principal} from '../pages/principal/principal'
import {Login} from '../pages/login/login';
import { Password } from '../pages/password/password';
import { Registrarse } from '../pages/registrarse/registrarse';
import { Busqueda} from '../pages/busqueda/busqueda';
import { DetalleLocal } from  '../pages/detalle-local/detalle-local';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Bienvenida;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Page One', component: Page1 },
      { title: 'Page Two', component: Page2 },
      {title: 'Bienvenida', component: Bienvenida},
      {title: 'Principal', component: Principal},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.backgroundColorByHexString('#ffffff');
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
