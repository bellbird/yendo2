import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {FormBuilder, Validators} from "@angular/forms";

/*
  Generated class for the Registrarse page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-registrarse',
  templateUrl: 'registrarse.html'
})
export class Registrarse {

  singupData;

  constructor(public navCtrl: NavController, fb: FormBuilder) {
    this.singupData=fb.group({
      username: ["", Validators.required],
      email:["", Validators.required],
      fecha_nacimiento:["", Validators.required],
      sexo: ["",Validators.required],
      password: ["", Validators.required]
    });
  }
  doRegistrar(){
    console.log(this.singupData);

  }

}
