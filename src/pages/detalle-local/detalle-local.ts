import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the DetalleLocal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-detalle-local',
  templateUrl: 'detalle-local.html'
})
export class DetalleLocal {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello DetalleLocal Page');
  }

}
