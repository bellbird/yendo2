import { Component } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import { NavController } from 'ionic-angular';
import { Principal } from '../principal/principal';
import { Password } from '../password/password';
import { Registrarse } from '../registrarse/registrarse';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
  principal = Principal;
  password_page = Password;
  registrarse_page= Registrarse;
  loginData;

  constructor(public navCtrl: NavController, fb: FormBuilder) {
    this.loginData=fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  doLogin(){
    console.log(this.loginData.value);
    this.navCtrl.setRoot(this.principal);
  }
  registrarse(){
    this.navCtrl.push(this.registrarse_page);
  }
  password(){
    this.navCtrl.push(this.password_page);
  }

}
