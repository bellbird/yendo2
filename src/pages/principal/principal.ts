import { Component } from '@angular/core';

import {NavController, Slides} from 'ionic-angular';
import {ViewChild} from "@angular/core/src/metadata/di";

@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html'
})
export class Principal {

  constructor(public navCtrl: NavController) {

  }

  @ViewChild('mySlider') sliderPrincipal: Slides;

  mySlideOptions = {
    initialSlide: 0,
    loop: true
  };

  next(){
      this.sliderPrincipal.slideNext();
  }

  prev(){
      this.sliderPrincipal.slidePrev();
  }


}
