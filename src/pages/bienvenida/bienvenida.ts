import { Component, ViewChild } from '@angular/core';
import {Slides, NavController} from 'ionic-angular';
import { Login } from '../login/login';



@Component({
  templateUrl: 'bienvenida.html'
})
export class Bienvenida {
  primero:boolean;
  ultimo:boolean;
  login = Login;

  constructor(public navCtrl: NavController){
    this.primero = true;
    this.ultimo = false;
  }

  @ViewChild('mySlider') slider: Slides;

  mySlideOptions = {
    initialSlide: 0
  };



  slides = [
    {
      title: "Descrubrí los beneficios e importantes descuentos que los locales tienen para vos.",
      image: "assets/img/slide1.png",
    },
    {
      title: "También podes dejar una reseña del local donde fuiste sobre la experiencia que tuviste.",
      image: "assets/img/slide2.png",
    },
    {
      title: "Ahorra tiempo y dinero haciendo tu reserva de lugar en el local que quieras.",
      image: "assets/img/slide3.png",
    }
  ];

  onSlideChanged() {
    this.primero = this.slider.isBeginning();
    this.ultimo = this.slider.isEnd();
  }

  next(){
    if (!this.slider.isEnd()){
      this.slider.slideNext();
    }
  }

  prev(){
    if (!this.slider.isBeginning()){
      this.slider.slidePrev();
    }
  }

  saltar(){
    this.navCtrl.push(this.login);
  }
}
