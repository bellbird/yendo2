import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Busqueda page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-busqueda',
  templateUrl: 'busqueda.html'
})
export class Busqueda {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello Busqueda Page');
  }

}
